package com.example.tabish.movieslist.Services

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.tabish.movieslist.Utilities.BASE_URL
import com.example.tabish.movieslist.Utilities.REQUEST_TOKEN_URL
import com.example.tabish.movieslist.Utilities.TMDB_API_KEY
import com.example.tabish.movieslist.Utilities.TOP_RATED_MOVIES_URL
import org.json.JSONObject
import java.util.*

object AuthService {

    fun getAllMovies(context: Context?, getMoviesCallback: (JSONObject) -> Unit) {

        // Request for all movies list.
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, BASE_URL + TOP_RATED_MOVIES_URL + "?api_key=${TMDB_API_KEY}", null, Response.Listener { response ->
            getMoviesCallback(response)
        },
        Response.ErrorListener { error ->
            Log.d("Error", "could not fetch movies ${error}")
        })

        Volley.newRequestQueue(context).add(jsonObjectRequest)
    }

    fun login(context: Context?) {

        // first getting request token
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, BASE_URL + REQUEST_TOKEN_URL + "?api_key=${TMDB_API_KEY}", null, Response.Listener { response ->

            // now requesting for user login
            

        },
        Response.ErrorListener { error ->
            Log.d("Error", "could not get request token: ${error}")
        })
    }

}