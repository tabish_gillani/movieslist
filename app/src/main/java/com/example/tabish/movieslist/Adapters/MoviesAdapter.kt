package com.example.tabish.movieslist.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.tabish.movieslist.Models.Movie
import com.example.tabish.movieslist.R
import com.example.tabish.movieslist.Services.CircleTransformService
import com.example.tabish.movieslist.Utilities.IMAGE_BASE_URL
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_list_item.view.*

class MoviesAdapter(val context: Context?, val movies: List<Movie>, val movieClicked: (Movie) -> Unit) : RecyclerView.Adapter<MoviesAdapter.MovieHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.movie_list_item, parent, false)
        return MovieHolder(view, movieClicked)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
       holder?.bindMovie(context, movies[position])
    }

    override fun getItemCount(): Int {
        return movies.count()
    }


    inner class MovieHolder(itemView: View?, movieClicked: (Movie) -> Unit) : RecyclerView.ViewHolder(itemView) {
        val movieName = itemView?.findViewById<TextView>(R.id.movieName)
        val movieRating = itemView?.findViewById<TextView>(R.id.movieRating)
        val movieReleaseYear = itemView?.findViewById<TextView>(R.id.releaseYear)
        val moviePoster = itemView?.findViewById<ImageView>(R.id.moviePoster)

        fun bindMovie(context: Context?, movie: Movie) {
            movieName?.text = movie.name
            movieRating?.text = "Avg Rating: "+movie.rating
            movieReleaseYear?.text = movie.releaseYear

            Picasso.get()
                    .load(IMAGE_BASE_URL + movie.posterUrl)
                    .transform(CircleTransformService(50,0))
                    .fit()
                    .into(moviePoster);

            itemView.setOnClickListener { movieClicked(movie) }

        }
    }
}