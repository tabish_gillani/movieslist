package com.example.tabish.movieslist.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tabish.movieslist.Adapters.MoviesAdapter
import com.example.tabish.movieslist.Controllers.MovieDetail
import com.example.tabish.movieslist.Models.Movie

import com.example.tabish.movieslist.R
import com.example.tabish.movieslist.Services.AuthService
import com.example.tabish.movieslist.Services.MovieDataService
import kotlinx.android.synthetic.main.fragment_popular_movies.*
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PopularMoviesFragment : Fragment() {

    lateinit var movieAdapter: MoviesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_popular_movies, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AuthService.getAllMovies(activity) { response ->
            println("in callback: ")
            var movieInfo: JSONObject
            var movie: Movie
            var moviesList = ArrayList<Movie>()

            for (index in 0..(response.getJSONArray("results").length() - 1)) {
                movieInfo = response.getJSONArray("results").getJSONObject(index)
                movie = Movie(movieInfo.getString("title"), movieInfo.getString("vote_average")
                        , movieInfo.getString("release_date"), movieInfo.getString("poster_path"),
                        movieInfo.getString("backdrop_path"), movieInfo.getString("overview"))
                moviesList.add(movie)
            }

            MovieDataService.movies.clear()
            MovieDataService.movies.addAll(moviesList)


            movieAdapter = MoviesAdapter(activity, MovieDataService.movies) { movie ->
                println("movie clicked: "+movie.name)
                val movieDetailIntent = Intent(activity, MovieDetail::class.java)
                movieDetailIntent.putExtra("movie", movie)
                startActivity(movieDetailIntent)
            }
            movieList.adapter = movieAdapter

            val layoutManager = LinearLayoutManager(activity)
            movieList.layoutManager = layoutManager
            movieList.setHasFixedSize(true)
        }
    }

}
