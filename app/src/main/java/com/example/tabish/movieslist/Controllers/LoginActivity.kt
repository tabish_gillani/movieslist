package com.example.tabish.movieslist.Controllers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.tabish.movieslist.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun login(view: View){
        if(emailTxt.text.isEmpty()) {
            Toast.makeText(this,"Enter Email Address.",Toast.LENGTH_SHORT).show()
        } else if(!emailTxt.text.contains("@")) {
            Toast.makeText(this,"Invalid Email Address !!",Toast.LENGTH_SHORT).show()
        } else if(passwordTxt.text.isEmpty()) {
            Toast.makeText(this,"Enter Password.",Toast.LENGTH_SHORT).show()
        } else if(passwordTxt.text.length < 8) {
            Toast.makeText(this,"Password should contains 8 characters.",Toast.LENGTH_SHORT).show()
        } else {
            
        }

    }
}
