package com.example.tabish.movieslist.Models

import android.os.Parcel
import android.os.Parcelable

class Movie(val name: String, val rating: String, val releaseYear: String, val posterUrl: String
    , val backdropPath: String, val overview: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(rating)
        parcel.writeString(releaseYear)
        parcel.writeString(posterUrl)
        parcel.writeString(backdropPath)
        parcel.writeString(overview)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }
    }
}