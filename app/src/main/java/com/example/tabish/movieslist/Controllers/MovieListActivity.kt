package com.example.tabish.movieslist.Controllers

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.example.tabish.movieslist.Adapters.MoviesAdapter
import com.example.tabish.movieslist.Fragments.MyListFragment
import com.example.tabish.movieslist.Fragments.PopularMoviesFragment
import com.example.tabish.movieslist.Models.Movie
import com.example.tabish.movieslist.R
import com.example.tabish.movieslist.Services.AuthService
import com.example.tabish.movieslist.Services.MovieDataService
import kotlinx.android.synthetic.main.activity_movie_list.*
import org.json.JSONObject

class MovieListActivity : AppCompatActivity() {

    lateinit var popularMoviesFragment: PopularMoviesFragment
    lateinit var myListFragment: MyListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)

        popularMoviesFragment = PopularMoviesFragment()
        myListFragment = MyListFragment()

        setFragment(popularMoviesFragment)

        navMain.setOnNavigationItemSelectedListener { item ->
            var response = false
            when (item.itemId) {
                R.id.nav_top_rated -> {
                    navMain.itemBackgroundResource = R.color.colorPrimary
                    setFragment(popularMoviesFragment)
                    response = true
                }
                R.id.nav_my_list -> {
                    navMain.itemBackgroundResource = R.color.colorAccent
                    setFragment(myListFragment)
                    response = true
                }
                R.id.nav_notif -> {
                    navMain.itemBackgroundResource = R.color.colorPrimaryDark
                    response = true
                }
            }

            response
        }

    }

    private fun setFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.mainLayout, fragment)
        fragmentTransaction.commit()
    }
}
