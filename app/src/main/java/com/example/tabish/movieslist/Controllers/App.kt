package com.example.tabish.movieslist.Controllers

import android.app.Application
import com.squareup.picasso.Picasso
import com.squareup.picasso.OkHttp3Downloader



class App : Application() {
    override fun onCreate() {
        super.onCreate()

        val picassoBuilder = Picasso.Builder(this)

        picassoBuilder.downloader(OkHttp3Downloader(this, Integer.MAX_VALUE.toLong()))
        val built = picassoBuilder.build()
//        built.setIndicatorsEnabled(true)
        built.setLoggingEnabled(true)
        Picasso.setSingletonInstance(built)
    }
}