package com.example.tabish.movieslist.Utilities

const val BASE_URL: String = "https://api.themoviedb.org/3/"
const val TOP_RATED_MOVIES_URL: String = "movie/top_rated"
const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/"
const val REQUEST_TOKEN_URL = "authentication/token/new"
const val TMDB_API_KEY = "1a1151f979394c9975064dc04c52d333"