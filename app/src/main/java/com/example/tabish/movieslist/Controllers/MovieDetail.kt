package com.example.tabish.movieslist.Controllers

import android.database.sqlite.SQLiteDatabase
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import com.example.tabish.movieslist.Models.Movie
import com.example.tabish.movieslist.R
import com.example.tabish.movieslist.Utilities.IMAGE_BASE_URL
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetail : AppCompatActivity() {

    lateinit var movie: Movie

    override fun openOrCreateDatabase(name: String?, mode: Int, factory: SQLiteDatabase.CursorFactory?): SQLiteDatabase {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        // adding actionbar
        val actionBar = supportActionBar
        actionBar?.title = "Detail"
        actionBar?.setDisplayHomeAsUpEnabled(true)

        movie = intent.getParcelableExtra<Movie>("movie")
        println("movie passed: "+ movie)

        movieName.text = movie.name
        movieOverview.text = movie.overview
        Picasso.get()
                .load(IMAGE_BASE_URL + movie.backdropPath)
                .into(movieBackdropImg);

        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        mToolbar.setTitle(movie.name)
        mToolbar.setNavigationIcon(R.drawable.abc_ic_arrow_drop_right_black_24dp)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
